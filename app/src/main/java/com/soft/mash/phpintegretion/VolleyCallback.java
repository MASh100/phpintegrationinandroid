package com.soft.mash.phpintegretion;

import org.json.JSONArray;

/**
 * Created by Shafi on 6/16/2016.
 */
public interface VolleyCallback {
    public void onSuccess(JSONArray response);
}
