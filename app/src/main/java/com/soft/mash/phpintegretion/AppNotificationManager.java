package com.soft.mash.phpintegretion;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

/**
 * Created by mash on 6/16/16.
 */
public class AppNotificationManager {
    public AppNotificationManager(Context context)
    {
        Intent intent = new Intent(context,NotificationReceiver.class);
        PendingIntent pIntent = PendingIntent.getActivity(context,(int) (System.currentTimeMillis()+2000), intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification noti = new Notification.Builder(context)
                .setContentTitle("New mail from " + "test@gmail.com")
                .setContentText("Subject").setSmallIcon(R.drawable.notification_icon)
                .setContentIntent(pIntent)
                .build();

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(01,noti);
    }
}
