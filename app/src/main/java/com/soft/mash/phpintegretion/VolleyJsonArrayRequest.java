package com.soft.mash.phpintegretion;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Map;

/**
 * Created by Shafi on 6/16/2016.
 */
public class VolleyJsonArrayRequest {

    public static void sendJSONObjectRequest(final Context context, final String url,@Nullable final Map<String,String> params, final VolleyCallback callback)
    {
        JsonArrayRequest object = new JsonArrayRequest(Request.Method.POST, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d("onResponse", response.toString());
                Toast.makeText(context, "onResponse" + response.toString(), Toast.LENGTH_SHORT).show();
                callback.onSuccess(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "Error with "+ url, Toast.LENGTH_SHORT).show();
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Toast.makeText(context, "Params are being sent:"+params.toString(), Toast.LENGTH_SHORT).show();
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(object);
    }

    public static void sendStringRequest(final Context context, final String url,@Nullable final Map<String,String> params, final VolleyCallback callback)
    {
        StringRequest object = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                JSONArray responed = null;
                try {
                    responed = new JSONArray(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                callback.onSuccess(responed);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "Error with "+ url, Toast.LENGTH_SHORT).show();
                error.printStackTrace();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(object);
    }
}
