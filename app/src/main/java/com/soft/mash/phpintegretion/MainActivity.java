package com.soft.mash.phpintegretion;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    JSONArray[] response1 = new JSONArray[1];
    Button button, notification;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = (Button) findViewById(R.id.btn_test);
        notification = (Button) findViewById(R.id.btn_notification);

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AppNotificationManager(MainActivity.this);
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String tableName = "Category";
                String url = "http://safi.mudkart.com/get"+tableName+"Ids.php";

                VolleyJsonArrayRequest.sendStringRequest(MainActivity.this, url, null, new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONArray response) {
                        response1[0] = response;
                        Toast.makeText(MainActivity.this, "onSuccess" + response.toString(), Toast.LENGTH_SHORT).show();
                        String ids = null;
                        try {
                            int length = response.length();
                            JSONObject object;
                            for (int i=0; i< length; i++) {
                                object = response.getJSONObject(i);
                                ids += object.get("id");
                            }
                            ids = response.getJSONObject(0).getString("id") + "," +
                                    response.getJSONObject(1).getString("id");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        String getChangeUrl = "http://safi.mudkart.com/get"+tableName+"Changes.php";
                        getAllColumns(ids, getChangeUrl);
                    }
                });
            }
        });

    }

    private void getAllColumns(String ids, String getChangeUrl) {

        Toast.makeText(MainActivity.this, "sending request for "+response1[0].toString(), Toast.LENGTH_SHORT).show();

        Map<String,String> params = new HashMap<>();
        //ids = "1,2";
        params.put("id",ids);

        VolleyJsonArrayRequest.sendStringRequest(this, getChangeUrl, params, new VolleyCallback() {
            @Override
            public void onSuccess(JSONArray response) {
                Log.d("Details:", response.toString());
                //insert into database
                try {
                    Log.d(response.getJSONObject(0).getString("name"),"first object");
                    Log.d(response.getJSONObject(1).getString("name"),"2nd object");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
